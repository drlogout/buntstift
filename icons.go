package buntstift




var unicode = map[string]string{
	"checkMark" : "\u2713",
	"crossMark" : "\u2717",
	"multiplicationDot" : "\u2219",
	"rightPointingPointer" : "\u25bb",
	"boxDrawingsLightHorizontal" : "\u2500",
}

var ascii = map[string]string{
	"checkMark": "+",
	"crossMark": "!",
	"multiplicationDot": "-",
	"rightPointingPointer": "?",
	"boxDrawingsLightHorizontal": "-",
}

func getIcons(isAscii bool) map[string]string{
	if isAscii {
		return ascii
	}
	return unicode
}